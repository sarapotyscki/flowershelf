/* 
 * Flowershelf
 *
 * Version: 1.1
 *  Author: Sara Potyscki (ThePot)
 * Website: http://www.thepot.site
 * License: Released under the MIT license [http://www.thepot.site]
 *
 */


(function (d,w) {
    'use strict';
		
	w.flowershelf = function(divContainer, options = null){
		if (options == null) {
			options = {"nothing": true};
		}
		return this.init(divContainer, options);
	};
	
	flowershelf.prototype = {
		_divContainer : null,
		_json : null,
		_callback : function(){},
		
		options : {
			gutter: 0,
			rowHeight: null,
			fullRows: true,
			json: null,
			orderedLoading: false,
			callbackFunction : function(){} 
		},
		init : function(divContainer, options){
			
			for(var i in options){
				this.options[i] = options[i];
			}
			this._divContainer = divContainer;
			
			this._divContainer.setAttribute("class",this._divContainer.getAttribute("class")+" loading");
			
			if (this.options.json) { 
				this._htmlCreation();
			}else{
				this._htmlLoading(); //loading img -> json creation -> render
				
			}
			
			
			var _this = this;
			window.onresize = function(){
				_this._onWindowResize();
			};
			
			
			
		},
		_htmlCreation : function(){
			this._json = this.options.json;
			
			for(var iRow = 0; iRow < this._json.length; iRow++){
				
				var nodeRow = document.createElement("div");
				nodeRow.addClass("flowershelf-row");
				this._divContainer.appendChild(nodeRow);
				
				var arrElem = this._json[iRow];
				for(var iElem = 0; iElem < arrElem.length; iElem++){
					var nodeElement = document.createElement("div");
					nodeRow.appendChild(nodeElement);
					for(var kAttr in arrElem[iElem].custom_attributes){
						nodeElement.setAttribute(kAttr,arrElem[iElem].custom_attributes[kAttr]);
					}
					
					nodeElement.addClass("loading");
					nodeElement.addClass("flowershelf-element");
					nodeElement.innerHTML = arrElem[iElem].custom_html;
					var nodeImage = document.createElement("img");
					
					
					//nodeElement.appendChild(nodeImage);
					nodeElement.insertBefore(nodeImage, nodeElement.children[0]);
					
					
					this._json[iRow][iElem].ref = nodeElement;
					
					if(!this.options.orderedLoading){
						this._loadImage(iRow,iElem);
					}
					
				}
			}
			
			if(this.options.orderedLoading){
				this._loadImages(0,0);
			}
			this._render();			
		},
		_htmlLoading : function(){
			var images = this._divContainer.querySelectorAll("img");
			var imageCount = images.length;
			var imagesLoaded = 0;
			
			var _this = this;
			
			for(var i=0; i<imageCount; i++){
				if(images[i].complete){
					imagesLoaded++;
				}else{
				
					images[i].onload = function(){
						imagesLoaded++;
						if(imagesLoaded == imageCount){
							_this._jsonFromHtml();	
						}
					};
				}
			}
			
			if(imagesLoaded == imageCount){
				_this._jsonFromHtml();	
			}	
		},
		_jsonFromHtml : function(){
			this._json = [];
			
			var arrRows = this._divContainer.children;
			for(var iRow = 0; iRow < arrRows.length; iRow++){
				var row = [];				
				arrRows[iRow].addClass("flowershelf-row");
				
				var arrElem = arrRows[iRow].children;
				for(var iElem = 0; iElem < arrElem.length; iElem++){
					
					arrElem[iElem].addClass("flowershelf-element");
					arrElem[iElem].addClass("loading");
					var element = {};
					
					var $img = arrElem[iElem].querySelector("img");
					element.width = $img.naturalWidth;
					element.height = $img.naturalHeight;
					element.image = $img.src;
					element.ref = arrElem[iElem];
					
					
					
					row.push(element);
				}
				this._json.push(row);
			}
			
			for(var iRow = 0; iRow < arrRows.length; iRow++){
				var arrElem = arrRows[iRow].children;
				for(var iElem = 0; iElem < arrElem.length; iElem++){
					this._loadImage(iRow, iElem);
				}
			}
			
			this._render();
		},
		_render : function(containerWidth){
			var _this = this;
			
			if (containerWidth == null) {
				containerWidth = Math.floor(this._divContainer.getBoundingClientRect().width);
			}
			
			for(var iRow = 0; iRow < this._json.length; iRow++){
				var arrElem = this._json[iRow];
				var rowWidthAtSameHeight = 0;
					
				if (this.options.rowHeight === null) {
						
					for(var iElem = 0; iElem < arrElem.length; iElem++){
						var $element = arrElem[iElem];
						var $img = $element.querySelector("img");
						
						rowWidthAtSameHeight += (($img.naturalWidth) * 1000 / ($img.naturalHeight));
					}
			
					var rowNewHeight = (containerWidth - (this.options.gutter * (arrElem.length - 1))) * 1000 / rowWidthAtSameHeight;
					for(var iElem = 0; iElem < arrElem.length; iElem++){
						var $element = arrElem[iElem];
						
						if (iElem != 0 - 1) {
							$element.addClass("first");
							if(this._checkContainerWidth(containerWidth)){
								return;
							}
							
						}
						if (iElem != arrElem.length - 1) {
							$element.addClass("last");
						}								
						$element.style.height = Math.floor(rowNewHeight) + "px";
						$element.style.width = Math.floor((arrElem[iElem].width * rowNewHeight / arrElem[iElem].height)) + "px";
						if (iElem != arrElem.length - 1) {
							$element.style.marginRight = this.options.gutter + "px";
						}else{
							$element.style.marginRight = "0px";
						}
						if (iRow != this._arrRows.length - 1) {
							$element.style.marginBottom = this.options.gutter + "px";
						}
					}
				}else{
						
					for(var iElem = 0; iElem < arrElem.length; iElem++){
						rowWidthAtSameHeight += ((arrElem[iElem].width) * this.options.rowHeight / (arrElem[iElem].height));
					}
					
					var subrowCount = Math.round(rowWidthAtSameHeight / containerWidth);
					var subrowLength = 0;
					var subrowBreaks = [];
					var subrowNewLength;
					
					if (!this.options.fullRows) {
						subrowNewLength = containerWidth;
					}else{
						subrowNewLength = rowWidthAtSameHeight / subrowCount;
					}
					
					var subrowBreakpoint = subrowNewLength;
					var currentInRow = 0;
					for(var iElem = 0; iElem < arrElem.length; iElem++){
						
						var precSubrowLength = subrowLength;
						subrowLength += ((arrElem[iElem].width) * this.options.rowHeight / (arrElem[iElem].height)) + this.options.gutter;
						
						var subgutteredSubrowLength = subrowLength - this.options.gutter;
						currentInRow++;
						if (subgutteredSubrowLength > subrowBreakpoint) {
							if ((subgutteredSubrowLength - subrowBreakpoint > subrowBreakpoint - precSubrowLength || !this.options.fullRows) && currentInRow > 1) {
								subrowBreaks.push(iElem-1); //prima
								currentInRow--;
								iElem--;
							}else{
								subrowBreaks.push(iElem); //dopo
							}
							
							if (!this.options.fullRows) {
								subrowLength = 0;
							}else{
								subrowBreakpoint += subrowNewLength;
							}
							currentInRow = 0;
						}else if (iElem == arrElem.length - 1){
							subrowBreaks.push(arrElem.length-1);
						}
						
					}
					
					
					var startPoint = 0;
					for(var k = 0; k < subrowBreaks.length; k++){
						var endPoint = subrowBreaks[k];
						
						var rowWidthAtSameHeight = 0;
						for(var iElem = startPoint; iElem <= endPoint; iElem++){
							rowWidthAtSameHeight += ((arrElem[iElem].width) * 1000 / (arrElem[iElem].height));
						}
	
						var rowNewHeight;
						
						
						if (!this.options.fullRows && k == subrowBreaks.length - 1) {
							if (startPoint == endPoint) {
								var elementWidth = ((arrElem[startPoint].width) * this.options.rowHeight / (arrElem[startPoint].height));
								if (elementWidth >= containerWidth) {
									rowNewHeight = (containerWidth) * 1000 / rowWidthAtSameHeight;
								}else{
									rowNewHeight = this.options.rowHeight;
								}
							}else{
								rowNewHeight = this.options.rowHeight;
							}
						}else{
							rowNewHeight = (containerWidth - (this.options.gutter * (endPoint - startPoint))) * 1000 / rowWidthAtSameHeight;
						}
						
						
						var currentRowWidth = 0;
						for(var iElem = startPoint; iElem <= endPoint; iElem++){
							var $element = arrElem[iElem].ref;
							
							var newWidth = Math.floor((arrElem[iElem].width * rowNewHeight / arrElem[iElem].height));
							currentRowWidth += newWidth;
							
							if (iElem == startPoint) {
								$element.addClass("first");
								if(this._checkContainerWidth(containerWidth)){
									return;
								}
							}
							if (iElem == endPoint) {
								$element.addClass("last");
								if (currentRowWidth != containerWidth && iElem != subrowBreaks[subrowBreaks.length-1]) {
									newWidth += containerWidth - currentRowWidth;
								}
							}
							
							$element.style.height = Math.floor(rowNewHeight) + "px";
							$element.style.width = newWidth + "px";
							
							if (iElem != endPoint) {
								$element.style.marginRight = this.options.gutter + "px";
								currentRowWidth += this.options.gutter;
							}else{
								$element.style.marginRight = "0px";
							}
							if (iElem <= subrowBreaks[subrowBreaks.length-2] || iRow < this._json.length-1) {
								$element.style.marginBottom = this.options.gutter + "px";
							}
						}
					
						startPoint = endPoint+1;
					
					}
				}
				
			}
			
			
			this._divContainer.setAttribute("class",this._divContainer.getAttribute("class").replace("loading", ""));
			
			this.options.callbackFunction();
		},
		_checkContainerWidth : function(usedWidth){
			var _this = this;
			if(this._divContainer.getBoundingClientRect().width < usedWidth){
				setTimeout(function(){
					_this._render(Math.floor(_this._divContainer.getBoundingClientRect().width));
				},1);
				return true;
			}else{
				return false;
			}
		},
		_onWindowResize : function(){
			this._divContainer.setAttribute("class",this._divContainer.getAttribute("class")+" loading");
			this._render();			
			
		},
		_loadImage : function(iRow, iElem){
			
			var element = this._json[iRow][iElem];
			var $element = element.ref;
			var $img = $element.querySelector("img");
			
			if ($img.getAttribute("src") == null) {
				$img.setAttribute("src",element.image);
			}
			
			if ($img.complete) {
				setTimeout(function(){
					$element.removeClass("loading");
					$element.addClass("loaded");
				},1);
			}else{
				$img.onload = function(){
					$element.removeClass("loading");
					$element.addClass("loaded");
				};
			}
		
			
		},
		_loadImages : function(iRow,iElem){
			var _this = this;
			if (this._json.length>0 && this._json[iRow].length>0) {
				
				var end = false;
				var element = this._json[iRow][iElem];
				var $element = element.ref;
				var $img = $element.querySelector("img")
				var iNextRow, iNextElem;
				
				if ($img.getAttribute("src") == null) {
					$img.setAttribute("src",element.image);
				}
				
				if (iElem < this._json[iRow].length-1) {
					iNextRow = iRow;
					iNextElem = iElem + 1;
				}else if (iRow < this._json.length-1) {
					iNextRow = iRow + 1;
					iNextElem = 0;
				}else{
					end = true;
				}
				
				if ($img.complete) {
					setTimeout(function(){
						$element.removeClass("loading");
						$element.addClass("loaded");
						if (!end) {
							_this._loadImages(iNextRow,iNextElem);
						}
						
					},1);
				}else{
					$img.onload = function(){
						$element.removeClass("loading");
						$element.addClass("loaded");
						if (!end) {
							_this._loadImages(iNextRow,iNextElem);
						}
					};
				}
			
			}	
		}
	};
	
	
})(document,window);

